# CTS-sdk

#### 介绍
## CTS 【City Twins Script】

CTS-SDK 是基于开源项目 ThreeJS 进行二次开发的三维WebGL应用框架，该框架优化了部分 ThreeJS 的使用方式和增添一些通用功能，旨在为开发者快速构建 WebGL三维应用。

我直接把threeJS属性赋值到了CTS，所以不需要再引用threeJS 库。虽然大了一些，你都搞三维了，模型不大，库才1M。

## 网站地址

> http://www.mrfox.wang/


## 安装

```node
npm install
```

## 启动

```node
 npm run start
```

## 发布

```node
npm run build

其他  需要拷贝 resources 文件夹到 public （vue项目）

vue2 import CTS from "./@cts.mini";

vue3 import "./@cts.mini";

CDN  <script src="../dist/@cts.mini.js"></script>
```
