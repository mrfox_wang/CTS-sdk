const path = require('path');

module.exports = {
    entry: './src/index.js',
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: '@cts.mini.js',
        library: {
            name: 'CTS',
            type: 'umd',
            export: 'default'
        }
    },
    // 确保mode不是production
}
