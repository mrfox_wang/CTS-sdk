# CTS-SDK
#### Introduction
## CTS (City Twins Script)
CTS-SDK is a 3D WebGL application framework based on the open-source project ThreeJS, with secondary development. This framework optimizes some of the usage methods of ThreeJS and adds some general functions, aiming to enable developers to quickly build WebGL 3D applications.
I directly assigned the properties of ThreeJS to CTS, so there is no need to reference the ThreeJS library again. Although it's a bit larger, if you're working with 3D, the model isn't big, and the library is only 1MB.
## Website Address
> http://www.mrfox.wang/
## Installation
```node
npm install
```
## Start
```node
npm run start
```
## Deployment
```node
npm run build

For other projects, you need to copy the 'resources' folder to the 'public' directory (for Vue projects).

Vue2: import CTS from "./@cts.mini";

Vue3: import "./@cts.mini"; 

CDN: <script src="../dist/@cts.mini.js"></script>
```
