import { EffectComposer, GammaCorrectionShader, OutlinePass, RenderPass, ShaderPass } from 'three/addons'
import { Raycaster, Vector2 } from 'three'

export default class MouseEvent {
  constructor(_viewer, _isSelect, _callback, _type = 'click') {
    this.viewer = _viewer
    this.isSelect = _isSelect
    this.callback = _callback
    this.type = _type
    if (_isSelect) {
      // 创建后处理对象EffectComposer，WebGL渲染器作为参数
      const composer = new EffectComposer(_viewer.renderer)
      const renderPass = new RenderPass(_viewer.scene, _viewer.camera)
      composer.addPass(renderPass)

      // 创建OutlinePass通道
      const v2 = new Vector2(_viewer.renderer.domElement.clientWidth, _viewer.renderer.domElement.clientHeight)
      const outlinePass = new OutlinePass(v2, _viewer.scene, _viewer.camera)
      outlinePass.visibleEdgeColor.set(0x00ffff)
      outlinePass.edgeThickness = 4
      outlinePass.edgeStrength = 6
      composer.addPass(outlinePass)
      // 创建伽马校正通道
      const gammaPass = new ShaderPass(GammaCorrectionShader)
      composer.addPass(gammaPass)
      _viewer.addAnimate(() => {
        composer.render()
      })
      this.outlinePass = outlinePass
    }
  }

  /**
     * 模型列表集合Object3D
     * @param _modelList
     */
  startSelect(_modelList) {
    this.modelList = _modelList
    // 开始绑定点击事件
    this.stopSelect()
    this.bingEvent = this._event.bind(this, this) // 会是一个新的函数，第一个this与第二个this不一样
    this.viewer.renderer.domElement.addEventListener(this.type, this.bingEvent)
  }

  /**
     * 关闭鼠标事件
     */
  stopSelect() {
    this.viewer.renderer.domElement.removeEventListener(this.type, this.bingEvent)// 第一个this与第二个this不一样
  }

  _event(that, e) {
    const raycaster = new Raycaster()
    const mouse = new Vector2()
    if (that.isSelect) that.outlinePass.selectedObjects = []
    mouse.x = (e.offsetX / that.viewer.renderer.domElement.clientWidth) * 2 - 1
    mouse.y = -(e.offsetY / that.viewer.renderer.domElement.clientHeight) * 2 + 1
    raycaster.setFromCamera(mouse, that.viewer.camera)
    const intersects = raycaster.intersectObject(that.modelList, true)
    if (intersects.length > 0 && intersects[0]) {
      // 修改材质，使其发光
      if (that.isSelect) that.outlinePass.selectedObjects = [intersects[0].object]
      intersects[0] && that.callback(intersects[0].object, intersects[0].point)
    }
  }
}
