import {TWEEN} from 'three/examples/jsm/libs/tween.module.min.js'
import * as THREE from 'three'

/**
 * 物体插值运动函数
 */
export default class AnimatedTracks {
  /**
   * 构建模型动画函数
   * @param _model
   * @param _firstPosition
   */
  constructor(_model, _callback, _rotationY = Math.PI / 2) {
    this.position = _model.position
    this.model = _model
    this.rotationY = _rotationY
    this.tweenList = []
    this.positionList = []
    this.callback = _callback
    this.isStart = false
  }

  /**
   * 创建轨迹动画插值
   * @param nextPoi 插入下一个点[x,y,z]数组传入
   * @param time 时间（毫秒）
   * @returns {*}
   */
  insertTween(nextPoi, speed = 5) {
    const nextPoiPosition = new THREE.Vector3(nextPoi[0], nextPoi[1], nextPoi[2])
    let time
    if (this.positionList[0]) time = this.positionList[0].distanceTo(nextPoiPosition) / speed * 10000
    else time = this.position.distanceTo(nextPoiPosition) / speed * 10000
    this.positionList.push(nextPoiPosition)
    let tempTween = this.createTween(this.model, {
      x: nextPoi[0],
      y: nextPoi[1],
      z: nextPoi[2]
    }, time)
    // // 绑定插值动画【最后一个绑定下一个】
    if (this.tweenList.length > 0) this.tweenList[this.tweenList.length - 1].chain(tempTween)
    this.tweenList.push(tempTween)
  }

  createTween(model, nextPoi, time, Easing = TWEEN.Easing.Linear.None) {
    let that = this
    //初始化tween，设置变化对象
    let tempTween = new TWEEN.Tween(that.position)
    //设置变化函数
    tempTween.easing(Easing)
    tempTween.to(nextPoi, time)
    //动画开始前计算角度
    tempTween.onStart(() => {
      let z = nextPoi.z - model.position.z
      let x = nextPoi.x - model.position.x
      let angle = Math.atan2(z, x)
      model.rotation.y = that.rotationY - angle
      //判断是否启动动画
      that.isStart = true
    })
    //动画开始后计算位置
    tempTween.onUpdate(() => {
      model.position.set(that.position.x, that.position.y, that.position.z)
      that.callback && that.callback(that.position)
    })
    return tempTween
  }

  /**
   * 开始播放动画
   */
  start() {
    if (this.tweenList.length > 0 && !this.isStart) {
      this.tweenList[0].start()
      this.isStart = true
    }
  }
}
