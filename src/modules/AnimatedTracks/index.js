import * as THREE from 'three';
import TWEEN from "@tweenjs/tween.js";

export default class AnimatedTracks {
  constructor (_model, _viewer, _showLine, _callback, _rotationY = Math.PI / 2) {
    this.model = _model;
    this.rotationY = _rotationY;
    this.tweenList = [];
    this.positionList = [];
    this.callback = _callback;
    this.isStart = false;
    this.viewer = _viewer;
    this.line = null;
    this.geometry = new THREE.BufferGeometry();
  }

  insertTween (nextPoi, time = 5000) {
    const nextPoiPosition = new THREE.Vector3().copy(nextPoi);
    this.positionList.push(nextPoiPosition);
    const tempTween = this.createTween(this.model, nextPoiPosition, time);
    this.tweenList.push(tempTween);
    if (!this.isStart) tempTween.start();
  }

  createTween (model, nextPoi, time, Easing = TWEEN.Easing.Linear.None) {
    const that = this;
    const tempTween = new TWEEN.Tween(model.position);
    tempTween.easing(Easing);
    tempTween.to(nextPoi, time);
    tempTween.onStart(() => {
      const angle = Math.atan2(nextPoi.z - model.position.z, nextPoi.x - model.position.x);
      model.rotation.y = that.rotationY - angle;
      that.isStart = true;
    });
    tempTween.onComplete(() => {
      that.isStart = false;
      const nextTween = that.tweenList.shift();
      if (nextTween) nextTween.start();
    });
    tempTween.onUpdate(() => {
      that.callback && that.callback(model.position);
    });
    return tempTween;
  }
}
