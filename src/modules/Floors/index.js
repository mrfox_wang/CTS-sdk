import {
  PlaneGeometry,
  GridHelper,
  MeshPhongMaterial,
  Mesh,
  TextureLoader,
  RepeatWrapping,
  Color, Vector2, Group,
} from 'three'
import { Water } from 'three/examples/jsm/objects/Water.js'
import { floorMaterial, waterMaterial } from './material'
import { Reflector } from 'three/addons'
import { sRGBEncoding } from 'three.path/examples/js/libs/three.module'
import { ReflectorMaterial } from './ReflectorMaterial'
import { ReflectorNew } from './Reflector'
import * as THREE from 'three'

/**
 * 地板地面类
 */
export default class Floors {
  constructor(_viewer) {
    this.viewer = _viewer
    this.scene = _viewer.scene
    this.panelsGroup = new Group()
    this.scene.add(this.panelsGroup)
  }

  createGrid(size = [10, 10], color1 = '#a1a1a1', color2 = '#a8a8a8') {
    const grid = new GridHelper(size[0], size[1], new Color(color1), new Color(color2))
    grid.position.set(0, 0, 0)
    this.scene.add(grid)
  }

  /**
   * 创建镜面对象
   * @param height 高度
   */
  createReflector(rectangle, option) {
    const geometry = new PlaneGeometry(rectangle[0], rectangle[1])
    const groundMirror = new Reflector(geometry, {
      clipBias: 0.003,
      textureWidth: window.innerWidth * window.devicePixelRatio,
      textureHeight: window.innerHeight * window.devicePixelRatio,
      encoding: sRGBEncoding,
      multisample: 0,
      ...option,
    })
    groundMirror.rotateX(-Math.PI / 2)
    this.scene.add(groundMirror)
    return groundMirror
  }

  createImgReflector(option) {
    const reflector = new ReflectorNew()
    const material = new ReflectorMaterial({
      reflectivity: option.reflectivity || 0.2,	//反射率
      mirror: option.mirror || 0.1,
      mixStrength: option.mixStrength || 9,
      color: new Color(option.color || '#ffffff'),
      map: new TextureLoader().load(option.map, function (texture) {
        texture.wrapS = texture.wrapT = RepeatWrapping
        texture.encoding = sRGBEncoding
        // 需要把色彩空间编码改一下，原因我上一篇说过的
        texture.encoding = THREE.sRGBEncoding
      }),
      normalMap: new TextureLoader().load(option.normalMap, function (texture) {
        texture.wrapS = texture.wrapT = RepeatWrapping
        texture.encoding = sRGBEncoding
      }),
      normalScale: new Vector2(5, 5),
      dithering: true,
    })
    material.uniforms.tReflect = reflector.renderTargetUniform
    material.uniforms.uMatrix = reflector.textureMatrixUniform
    const geometry = new PlaneGeometry(option.width || 100, option.height || 100)
    const meshOB = new Mesh(geometry, material)
    meshOB.name = 'reflectorShaderMesh'
    meshOB.position.y = -.01
    meshOB.rotation.x = -Math.PI / 2
    meshOB.add(reflector)
    meshOB.onBeforeRender = (rendererSelf, sceneSelf, cameraSelf) => {
      meshOB.visible = false
      reflector.update(rendererSelf, sceneSelf, cameraSelf)
      meshOB.visible = true
    }
    this.scene.add(meshOB)
    meshOB.setReflectivity = val => {
      material.uniforms.uReflectivity.value = val
    }
    meshOB.setMirror = val => {
      material.uniforms.uMirror.value = val
    }
    meshOB.setMixStrength = val => {
      material.uniforms.uMixStrength.value = val
    }
    meshOB.setColor = val => {
      material.uniforms.uColor.value = new Color(val)
    }
    return meshOB
  }


  removePanelslAll() {
    this.panelsGroup.remove(...this.panelsGroup.children)
  }

  /**
   * 地面，设置地面材质
   * @param rectangle 矩形长宽【100，100】
   * @param height 高度为默认为0
   * @param size 材质重复贴图【100，100】
   * @param img 材质路径
   * @param isReflector 是否镜像
   */
  createPanel(option = {}) {
    const { castShadow, position, url, width, height, repeatX, repeatY } = option
    const geometry = new PlaneGeometry(width, height)
    // 水平、垂直重复次数*
    const material = new MeshPhongMaterial({
      map: new TextureLoader().load(url, function (texture) {
        texture.wrapS = texture.wrapT = RepeatWrapping
        texture.repeat.set(repeatX, repeatY)
      }),
      transparent: true,
      side: THREE.DoubleSide,
      depthWrite: false, // 无法被选择，鼠标穿透
      depthTest:true,
    })
    const mesh = new Mesh(geometry, material)
    mesh.receiveShadow = castShadow
    // mesh.castShadow = castShadow
    mesh.rotation.x = -Math.PI / 2
    mesh.position.set(position.x, position.y, position.z)
    // 设置网格的renderOrder
    mesh.renderOrder = 1 // 设置为1，这意味着它将在默认的renderOrder为0的对象之后渲染
    this.panelsGroup.add(mesh)
    return mesh
  }

  createWater(option = {}) {
    const waterGeometry = new PlaneGeometry(option.width || 100, option.height || 100)
    this.waterNormals = new TextureLoader().load(waterMaterial, function (texture) {
      texture.wrapS = texture.wrapT = RepeatWrapping
    }),
    this.water = new Water(
      waterGeometry, {
        textureWidth: 512,
        textureHeight: 512,
        waterNormals: this.waterNormals,
        waterColor: 0x001e0f,
        distortionScale: 3.7,
        fog: this.viewer.scene.fog !== undefined,
      },
    )
    this.water.rotation.x = -Math.PI / 2
    this.water.receiveShadow = true
    this.water.position.set(option.position.x, option.position.y, option.position.z)
    this.water.material.uniforms.sunDirection.value.normalize()
    this.scene.add(this.water)
    this.waterSpeed = option.speed || 1
    this.waterRun = () => {
      this.water.material.uniforms.time.value += this.waterSpeed / 1000
    }
    this.viewer.addAnimate(this.waterRun)
    return this.water
  }

  removeWater() {
    this.viewer.removeAnimate(this.waterRun)
    this.scene.remove(this.water)
    this.water = null
  }

  setWater(option) {

    if (!this.water) {
      this.createWater(option)
    } else {
      const waterUniforms = this.water.material.uniforms
      if (option.scale) {
        waterUniforms.size.value = option.scale
      }
      if (option.distortionScale) {
        waterUniforms.distortionScale.value = option.distortionScale
      }
      if (option.color) {
        waterUniforms.waterColor.value = new Color(option.color)
      }
      // 更新水位置
      if (option.position) {
        this.water.position.set(option.position.x, option.position.y, option.position.z)
      }
      // 更新水波纹速度
      if (option.speed) {
        this.waterSpeed = option.speed
      }
      // 更新水面大小
      if (option.width || option.height) {
        // 创建新的水几何体
        const newWaterGeometry = new PlaneGeometry(option.width || this.water.geometry.parameters.width, option.height || this.water.geometry.parameters.height)
        // 更新水几何体
        this.water.geometry.dispose() // 销毁旧的几何体
        this.water.geometry = newWaterGeometry
        // 重新设置水材质
        this.water.material.uniforms.normalSampler.value = this.water.material.uniforms.normalSampler.value.clone()
        this.water.material.uniforms.normalSampler.value.needsUpdate = true
      }
    }
  }
}
