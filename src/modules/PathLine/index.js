import * as THREE from 'three'
import { PathGeometry, PathPointList } from 'three.path'
import { Color, Group } from 'three'
import { createFlowWallMat, creatWallByPath } from './wall.js'

/**
 * 物体插值运动函数
 */
export default class PathLine {
  /**
   * 构建模型动画函数
   * @param _model
   * @param _firstPosition
   */
  constructor(_viewer) {
    this.viewer = _viewer
    this.linesGroup = new Group()
    _viewer.scene.add(this.linesGroup)
  }

  removeAll() {
    this.linesGroup.remove(...this.linesGroup.children)
  }


  loadRoad(option) {
    let paths = []
    option.paths.forEach(item => {
      paths.push(new THREE.Vector3(item[0], item[1], item[2]))
    })
    // 添加路径
    const up = new THREE.Vector3(0, 1, 0)
    const pathPointList = new PathPointList()
    pathPointList.set(paths, 0.1, 10, up, false)
    const geometry = new PathGeometry()// 物体的结构
    geometry.update(pathPointList, {
      width: option.width,
      arrow: false,
    })
    const material = new THREE.MeshBasicMaterial({ //  Phong网格材质
      // 颜色贴图
      map: option.url ? new THREE.TextureLoader().load(option.url, function (texture) {
        texture.wrapS = texture.wrapT = THREE.RepeatWrapping
        texture.repeat.set(option.repeatX, option.repeatY)
      }) : null,
      depthWrite: false,
      transparent: true,
      color: new Color(option.color),
    })
    // 网格
    let mesh = new THREE.Mesh(geometry, material)
    mesh.castShadow = true
    mesh.renderOrder = 99 // 设置为1，这意味着它将在默认的renderOrder为0的对象之后渲染
    if (!option.notAdd) this.linesGroup.add(mesh)// 网格添加到场景里面
    this.viewer.addAnimate(() => {
      material.map.offset.x -= option.speed
    })
    return mesh
  }

  // 创建水管路径
  _createPath(pointsArr) {
    // 自定义三维路径 curvePath
    const path = new THREE.CurvePath()
    for (let i = 0; i < pointsArr.length - 1; i++) {
      // 每两个点之间形成一条三维直线
      const lineCurve = new THREE.LineCurve3(
        pointsArr[i],
        pointsArr[i + 1],
      )
      // curvePath有一个curves属性，里面存放组成该三维路径的各个子路径
      path.curves.push(lineCurve)
    }
    return path
  }

  // 创建抛物线路径
  _createFlyPath(pointsArr, option) {
    const { num, maxY } = option
    // 自定义三维路径 curvePath
    const path = new THREE.CurvePath()
    const point1 = pointsArr[0]
    const point2 = pointsArr[1]
    const controlPoint = new THREE.Vector3(
      (point1.x + point2.x) / 2,
      maxY, // 控制点在最大高度
      (point1.z + point2.z) / 2,
    )

    // 创建一个二次贝塞尔曲线
    const quadraticBezierCurve = new THREE.QuadraticBezierCurve3(
      point1,
      controlPoint,
      point2,
    )

    // 将曲线添加到路径中
    path.curves.push(quadraticBezierCurve)

    return path
  }

  loadWaterRoad(option) {
    let paths = []
    option.paths.forEach(item => {
      paths.push(new THREE.Vector3(item[0], item[1], item[2]))
    })
    let path = option.type == 3 ? this._createFlyPath(paths, option) : this._createPath(paths, option)
    // 创建水管几何体
    const geometry = new THREE.TubeGeometry(path, 256, option.width, 12)
    // 创建材质
    const textureLoader = new THREE.TextureLoader()
    const texture = textureLoader.load(option.url)
    texture.wrapS = texture.wrapT = THREE.RepeatWrapping
    texture.repeat.set(option.repeatX, option.repeatY)
    const material = new THREE.MeshBasicMaterial({
      map: texture,
      depthWrite: false,
      transparent: true,
      color: new THREE.Color(option.color),
      side: THREE.DoubleSide,
    })
    let mesh = new THREE.Mesh(geometry, material)
    mesh.castShadow = true
    mesh.renderOrder = 99 // 设置为1，这意味着它将在默认的renderOrder为0的对象之后渲染
    this.linesGroup.add(mesh)// 网格添加到场景里面
    this.viewer.addAnimate(() => {
      material.map.offset.x -= option.speed
    })
  }

  loadWall(option) {
    let paths = option.paths
    const first = option.paths[0]
    paths.push(first)
    const material = createFlowWallMat(option)
    const mesh = creatWallByPath({
      height: option.width,
      path: option.paths,
      material,
      expand: true,
    })
    mesh.castShadow = true
    mesh.renderOrder = 99 // 设置为1，这意味着它将在默认的renderOrder为0的对象之后渲染
    this.linesGroup.add(mesh)// 网格添加到场景里面
    this.viewer.addAnimate(() => {
      material.map.offset.y -= option.speed
    })
  }
}
