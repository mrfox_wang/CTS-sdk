import { Mesh, MeshBasicMaterial, RepeatWrapping, SphereGeometry, TextureLoader } from 'three'
import * as THREE from 'three'

/**
 * 全景图功能
 */
export default class PanoramaMaps {
  constructor(_viewer, _option = {}) {
    this.viewer = _viewer
    this.option = _option
    this._createPanoramaMaps()
    this.start()
  }

  _createPanoramaMaps() {
    // 创建球体几何体
    const geometry = new SphereGeometry(500, 32, 32)
    // 随机颜色
    const material = new MeshBasicMaterial({
      map: new TextureLoader().load(this.option.map, function (texture) {
        texture.wrapS = texture.wrapT = RepeatWrapping
        // uv两个方向纹理重复数量
        texture.repeat.set(1, 1)
        // 需要把色彩空间编码改一下，原因我上一篇说过的
        texture.encoding = THREE.sRGBEncoding
      }),
      side: THREE.DoubleSide,
    })
    // 创建网格（球体）
    const sphere = new Mesh(geometry, material)
    this.viewer.scene.add(sphere)
  }

  start() {
    //设置360全景相机位置
    // this.viewer.camera.position.set(0, 0, 0) //设置相机位置
    // 参数及其默认值
    this.viewer.controls.enablePan = false       // 启用平移，默认为true
    this.viewer.controls.enableZoom = false      // 启用缩放，默认为true
    this.viewer.controls.enableRotate = true    // 启用旋转，默认为true

    this.viewer.controls.enableDamping = true   // 启用阻尼（惯性），使操作更平滑，默认为false
    this.viewer.controls.dampingFactor = 0.25    // 阻尼因子，默认为0.25
  }
}
