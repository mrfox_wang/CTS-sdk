// import { TilesRenderer } from '3d-tiles-renderer'

/**
 * 加载3D Tiles
 */
export default class TilesLoader {
  /**
   * 构造函数
   * @param viewer 场景对象添加
   */
  constructor(_viewer, url) {
    this.viewer = _viewer
    this.scene = _viewer.scene
    const tilesRenderer = new TilesRenderer(url)
    tilesRenderer.setCamera(this.viewer.camera)
    tilesRenderer.setResolutionFromRenderer(this.viewer.camera, this.viewer.renderer)
    tilesRenderer.group.position.set(0, 0, 0)
    // tilesRenderer.group.rotation.set(-Math.PI / 2, 0, 0)
    this.viewer.scene.add(tilesRenderer.group)
    return tilesRenderer.group
    this.viewer.addAnimate(() => {
      tilesRenderer.update()
    })
  }
}
