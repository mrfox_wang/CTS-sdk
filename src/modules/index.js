import Viewer from './Viewer'
import SkyBoxs from './SkyBoxs'
import ModelLoder from './ModelLoder'
import DsModel from './DsModel'
import Weather from './Weather'
import AnimatedTracks from './AnimatedTracks'
import Lights from './Lights'
import SunLensFlare from './Lights/SunLensFlare.js'
import MouseEvent from './MouseEvent'
import Labels from './Labels'
import PathLine from './PathLine'
import Floors from './Floors'
import FirstPersonMove from './FirstPersonMove'
import WeatherEffects from './WeatherEffects'
import GeojsonMap from './GeojsonMap'
import PanoramaMaps from './PanoramaMaps'
import FPS from './FPS'
import TilesLoader from './TilesLoader'

const modules = {
  Viewer, // 场景初始化
  SkyBoxs, // 天空盒
  ModelLoder, // 模型加载，现在主要是针对Gltf或者glb
  DsModel, // 模型功能,
  Lights, // 灯光管理
  Weather, // 天气控制
  AnimatedTracks, // 模型动画
  MouseEvent, // 鼠标事件
  Labels, // 场景标签
  PathLine, // 路线标签
  Floors, // 场景地板
  FirstPersonMove, //第一人称
  WeatherEffects,
  SunLensFlare, //太阳炫光
  GeojsonMap, // geojson加载
  PanoramaMaps, // 360全景
  FPS, // FPS
  TilesLoader // 3dtiles加载,倾斜模型
}

export default modules
