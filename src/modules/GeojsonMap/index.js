import * as THREE from 'three'
import * as d3 from 'd3'
import { Color, MeshPhongMaterial, RepeatWrapping, TextureLoader } from 'three'
import { mashMaterial } from './material'
import PathLine from '../PathLine/index'
import { PathGeometry, PathPointList } from 'three.path'
import * as turf from '@turf/turf'
import Labels from '../Labels/index'

export default class GeoJsonLoader {
  constructor(_viewer) {
    this.viewer = _viewer
    //坐标转换器
    this.GeoGroup = new THREE.Object3D()
    this.GeoGroup.castShadow = true
    this.viewer.scene.add(this.GeoGroup)
    //面的组
    this.meshGroup = new THREE.Object3D()
    this.GeoGroup.add(this.meshGroup)
    this.meshGroup.rotation.x = -Math.PI / 2
    //线的组
    this.lineGroup = new THREE.Object3D()
    this.GeoGroup.add(this.lineGroup)
    this.lineGroup.rotation.x = -Math.PI / 2
    this.pathLine = new PathLine(_viewer)
    this.labels = new Labels(_viewer)//全局的标签
    //柱状图单独展示
    this.boxGroup = new THREE.Object3D()
    this.boxGroup.rotation.x = -Math.PI / 2
    this.GeoGroup.add(this.boxGroup)
  }

  removeAll() {
    this.meshGroup.remove(...this.meshGroup.children)
    this.lineGroup.remove(...this.lineGroup.children)
    this.boxGroup.remove(...this.boxGroup.children)
    this.labels.removeLabelAll()//移除所有label
    this.pathLine.removeAll()
  }

  create(geoJson, option) {
    this.labels.removeLabelAll()//移除所有label
    this.projection = d3.geoMercator().center(option.center).scale(option.scale).translate([0, 0])
    this._createMaterial(option.material)
    this._loadGeoJson(geoJson.features, option)
  }

  _createMaterial(option) {
    const materialFront = new MeshPhongMaterial({
      map: option.url ? new TextureLoader().load(option.url || mashMaterial, function (texture) {
        texture.wrapS = texture.wrapT = RepeatWrapping
        // uv两个方向纹理重复数量
        texture.repeat.set(option.repeatX || 0, option.repeatY || 0)
        texture.offset.set(option.offsetX || 0, option.offsetY || 0)
      }) : null,
      transparent: true,
      depthTest: true,
    })
    materialFront.map.colorSpace = 'srgb'
    const materialSide = new THREE.MeshBasicMaterial({ color: new Color(option.color) })
    this.materials = [materialFront, materialSide]
  }

  /**
   * 加载·geojson
   * @param features 标准的geojson数据
   * @private
   */
  _loadGeoJson(features, option) {
    this.features = features //全局使用
    this.$options = option
    features.forEach(feature => {
      const coordinates = feature.geometry.coordinates
      const name = feature.properties.name || feature.properties.Street
      const group = new THREE.Object3D()
      group.name = name
      //绘制实体
      if (feature.geometry.type == 'MultiPolygon') {
        coordinates.forEach(coordinate => {
          this._createMeshLine(name, coordinate, group, option)
        })
      }
      if (feature.geometry.type == 'Polygon') {
        this._createMeshLine(name, coordinates, group, option)
      }
      this.meshGroup.add(group)
    })
  }

  /**
   * 添加柱状图
   * @param option
   */
  createBarChart(option) {
    this.boxGroup.remove(...this.boxGroup.children)
    //构建材质
    const shaderMaterial = new THREE.ShaderMaterial({
      uniforms: {
        colorStart: { value: new THREE.Color(option.barChart && option.barChart.topColor || '#FFF') }, // 顶部颜色：白色
        colorEnd: { value: new THREE.Color(option.barChart && option.barChart.bottomColor || '#af5210') },  // 底部颜色：自定义颜色
      },
      vertexShader: `
    varying vec3 vPosition;
    void main() {
      vPosition = position;
      gl_Position = projectionMatrix * modelViewMatrix * vec4(position, 1.0);
    }`,
      fragmentShader: `
    varying vec3 vPosition;
    uniform vec3 colorStart;
    uniform vec3 colorEnd;
    void main() {
      // 使用uniform变量定义渐变颜色
      float height = vPosition.y + 1.0; // 调整y坐标范围到[0, 2]
      float gradient = clamp(height / 2.0, 0.0, 1.0); // 计算渐变因子
      vec3 color = mix(colorStart, colorEnd, gradient); // 混合颜色
      gl_FragColor = vec4(color, 1.0);
    }`,
      side: THREE.DoubleSide, // 渲染柱状体的两侧
      depthTest: false,
      transparent: true,
    })
    if (this.features) this.features.forEach(feature => {
      const name = feature.properties.name || feature.properties.Street //兼容datav数据和自己下载的全国县市数据
      const cylinder = this._drawCylinder(name, feature, option.barChart, shaderMaterial)
      if (cylinder) this.boxGroup.add(cylinder)
    })
  }

  _createMeshLine(name, coordinates, group, option) {
    coordinates.forEach(polygon => {
      //底部面
      const mesh = this.drawExtrudeMesh(polygon, option)
      mesh.name = name
      group.add(mesh)

      //顶部线段
      const { depth, offsetX } = option.mesh
      const line = this.lineDraw(polygon, depth, offsetX, option.line)
      line.name = name
      group.add(line)

      const line1 = this.lineDraw(polygon, 0, offsetX, option.line)
      this.lineGroup.add(line1)
    })
  }

  _drawCylinder(name, feature, _option, shaderMaterial) {
    if (_option.width == null || _option.width == 0 ) return
    let value = Math.floor(Math.random() * 4) + 1
    let centroid = feature.properties.centroid
    if (!centroid) centroid = turf.centroid(feature).geometry.coordinates
    const [x, y] = this.projection(centroid)

    const geometry = new THREE.BoxGeometry(_option.width, value, _option.width)
    // 创建柱状图网格
    const barMesh = new THREE.Mesh(geometry, shaderMaterial)
    const { depth, offsetX } = this.$options.mesh
    barMesh.rotation.set(-Math.PI / 2, 0, 0)
    barMesh.position.set(x - 0.2, -y + 0.2, depth + offsetX + value / 2)
    barMesh.renderOrder = 999 // 设置渲染顺序，确保它不会被其他物体遮挡
    return barMesh
  }

  createTitle(option){
    if (this.features) this.features.forEach(feature => {
      const name = feature.properties.name || feature.properties.Street //兼容datav数据和自己下载的全国县市数据
      //绘制标签
      const label = this._drawTxt(name, feature, option)
      if (label) this.boxGroup.add(label)
    })

  }

  _drawTxt(name, feature, _option) {
    const { depth, offsetX } = this.$options.mesh
    let centroid = feature.properties.centroid
    if (!centroid) centroid = turf.centroid(feature).geometry.coordinates
    const [x, y] = this.projection(centroid)
    let div = document.createElement('div')
    div.style.pointerEvents = 'all'
    div.style.cursor = 'pointer'
    div.style.color = _option.title.color || 'rgba(255,255,255,0.7)'
    div.style.fontWeight = '600'
    div.style.fontSize = `${_option.title.size }px` || '14px'
    div.style.userSelect = 'none'
    div.style.display = 'none'
    div.innerHTML = name
    let option = {
      div,
      position: { x: x, y: -y, z: depth + offsetX },
      rotation: { x: 0, y: 0, z: 0 },
      type: '2d',
    }
    let label2d = this.labels.addDivLabel(option)
    return label2d
  }

  lineDraw(polygon, height = 0, offset = 0, option = {}) {
    let paths = []
    // 创建线条
    polygon.forEach(row => {
      const [x, y] = this.projection(row)
      // 创建三维点
      paths.push(new THREE.Vector3(x, -y, height + offset))
    })
    // 添加路径
    const up = new THREE.Vector3(0, 0, 1)
    const pathPointList = new PathPointList()
    pathPointList.set(paths, 0.1, 10, up, false)
    const geometry = new PathGeometry()// 物体的结构
    geometry.update(pathPointList, {
      width: option.width / 100,
      arrow: false,
    })
    const material = new THREE.MeshBasicMaterial({ //  Phong网格材质
      color: new Color(option.color),
    })
    // 网格
    let mesh = new THREE.Mesh(geometry, material)
    return mesh
  }

  drawExtrudeMesh(polygon, option) {
    const shape = new THREE.Shape()
    polygon.forEach((row, i) => {
      const [x, y] = this.projection(row)
      if (i === 0) {
        // 创建起点,使用moveTo方法
        // 因为计算出来的y是反过来，所以要进行颠倒
        shape.moveTo(x, -y)
      }
      shape.lineTo(x, -y)
    })
    // 拉伸
    const geometry = new THREE.ExtrudeGeometry(shape, {
      depth: option.mesh.depth * 3,
      bevelEnabled: false,
    })
    const mesh = new THREE.Mesh(geometry, this.materials)
    mesh.position.z = -option.mesh.depth * 2
    return mesh
  }
}
