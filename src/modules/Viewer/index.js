import {
  PerspectiveCamera,
  Scene,
  Color,
  Clock, Vector3, Box3,
} from 'three'
// 二维标签渲染器
import {OrbitControls} from 'three/examples/jsm/controls/OrbitControls.js'
import Stats from 'three/examples/jsm/libs/stats.module.js'
import MouseEvent from '../MouseEvent'
import SkyBoxs from '../SkyBoxs'
import Lights from '../Lights'
import * as THREE from 'three'
import TWEEN from '@tweenjs/tween.js'

export default class Viewer {
  /**
   * 构造函数
   * @param id 场景窗体div
   */
  constructor(id, option = {sky: true, light: true}) {
    this.id = id
    this.renderer = undefined
    this.scene = undefined
    this.camera = undefined
    this.controls = undefined
    this.controlsEnabled = true //开启控制器
    this.stats = undefined
    this.animateEventSet = new Set()
    this.clock = new Clock()
    this.TWEEN = TWEEN
    //执行函数
    this._init(option)
  }

  /**
   * 添加状态监测
   */
  addStats() {
    if (!this.stats) this.stats = new Stats()
    this.stats.dom.style.position = 'absolute'
    this.stats.dom.style.right = '0px'
    this.stats.dom.style.left = 'unset'
    if (!this.stats.run) {
      this.stats.run = true
      this.viewerDom.appendChild(this.stats.dom)
      this.statsFunction = () => {
        this._statsUpdate(this.stats)
      }
      this.addAnimate(this.statsFunction)
    }
  }

  removeStats() {
    if (this.stats.run) {
      this.stats.run = false
      this.viewerDom.removeChild(this.stats.dom)
      this.removeAnimate(this.statsFunction)
    }
  }

  destroy() {
    cancelAnimationFrame(this.animationFrame)
    this.scene.traverse(child => {
      try {
        if (child.material) {
          child.material.dispose()
        }
        if (child.geometry) {
          child.geometry.dispose()
        }
        child = null
      } catch {
      }
    })
    this.renderer.forceContextLoss()
    this.renderer.dispose()
    this.scene.clear()
    // 清理相机和渲染器
    this.camera = null
    this.renderer.domElement = null
    this.renderer = null
  }

  /**
   * 开启鼠标事件
   * @param mouseType
   * @param isSelect
   * @param callback
   */
  startSelectEvent(mouseType, isSelect, callback, modelList = this.scene) {
    this.mouseEvent = new MouseEvent(this, isSelect, callback, mouseType)
    this.mouseEvent.startSelect(modelList)
  }

  /**
   * 关闭鼠标事件
   */
  stopSelectEvent() {
    if (this.mouseEvent) {
      this.mouseEvent.stopSelect()
    }
  }


  /**
   * 状态更新
   * @param stats
   */
  _statsUpdate(stats) {
    stats.update()
  }

  _init(option) {
    this._initRenderer()
    // 渲染相机
    this._initCamera()
    // 渲染场景
    this._initScene()
    // 控制器
    this._initControl()
    // 天空盒
    if (option.sky) this._initSkybox()
    // 环境光
    if (option.light) this._initLight()
    // 渲染场景
    this._startAnimationLoop()
  }

  /**
   * 创建初始化场景界面
   */
  _initRenderer() {
    // 获取画布dom
    this.viewerDom = document.getElementById(this.id)
    if (!this.viewerDom) this.viewerDom = this.id //可以传入dom 对象
    this.viewerDom.style.position = 'relative'
    // 初始化渲染器
    this.renderer = new THREE.WebGLRenderer({
      antialias: true,
      logarithmicDepthBuffer: true,
      preserveDrawingBuffer: true,
      alpha: true,
    })
    this.renderer.shadowMap.enabled = true
    this.renderer.shadowMap.type = THREE.VSMShadowMap
    this.renderer.physicallyCorrectLights = true
    this.viewerDom.appendChild(this.renderer.domElement)// 一个canvas，渲染器在其上绘制输出。
    // eslint-disable-next-line @typescript-eslint/no-this-alias
    const that = this
    this.renderer.domElement.addEventListener('webglcontextrestored', function () {
      // 在这里重新初始化你的场景和渲染器
      that.destroy()
    }, false)
    // 子元素的事件监听器
    this.renderer.domElement.addEventListener('mousedown', function (event) {
      event.stopPropagation() // 阻止事件冒泡到父元素
      // 这里处理子元素的逻辑
    })
    // 禁止鼠标右键
    this.renderer.domElement.addEventListener('contextmenu', function (event) {
      event.preventDefault() // 阻止默认的右键菜单
      event.stopPropagation() // 阻止事件冒泡到父组件
    })

  }

  _initCamera() {
    // 渲染相机
    this.camera = new PerspectiveCamera(45, this.viewerDom.clientWidth / this.viewerDom.clientHeight, 0.1, 5000)
    this.camera.position.set(0, 0, 20) //设置相机位置
  }

  _initScene() {
    // 渲染场景
    this.scene = new Scene()
  }

  /**
   * 设置背景颜色
   * @param color
   */
  setBackground(color = 'rgb(85,151,196)') {
    if (color === 'rgba(0, 0, 0, 0)') this.scene.background = null
    else this.scene.background = new Color(color)
  }

  addControlsEventListener(_callback) {
    this.callbackControls = _callback
  }

  _initControl() {
    this.controls = new OrbitControls(this.camera, this.renderer.domElement)
    this.controls.enableDamping = true
    this.controls.dampingFactor = 0.2 // 范围通常在 0 到 1 之间
    this.controls.screenSpacePanning = false // 定义平移时如何平移相机的位置 控制不上下移动
    // eslint-disable-next-line @typescript-eslint/no-this-alias
    const that = this
    this.controls.addEventListener('change', function () {
      if (that.callbackControls) that.callbackControls({
        cameraPoint: that.camera.position,
        cameraTarget: that.controls.target,
      })
    })
  }

  /**
   * 全局动画事件
   * @private
   */
  _startAnimationLoop() {
    // eslint-disable-next-line @typescript-eslint/no-this-alias
    const that = this

    function animate() {
      that.animationFrame = requestAnimationFrame(animate)
      that._readerDom()
      that._undateDom() //一直更新界面大小，测试了，感觉没什么性能损耗，用性能来搞简单些嘛
      // 全局的公共动画函数，添加函数可同步执行
      that._executeAnimateEvents()
      // 更新tween动画
      TWEEN.update()
    }

    animate()
  }

  addAnimate(animate) {
    this.animateEventSet.add(animate)
  }

  removeAnimate(animate) {
    this.animateEventSet.delete(animate)
  }

  /**
   * 执行添加的函数
   * @private
   */
  _executeAnimateEvents() {
    this.animateEventSet.forEach(event => {
      event && event()
    })
  }


  // 更新dom大小
  _undateDom() {
    // eslint-disable-next-line @typescript-eslint/no-this-alias
    const that = this
    // 更新参数
    that.camera.aspect = that.viewerDom.clientWidth / that.viewerDom.clientHeight // 摄像机视锥体的长宽比，通常是使用画布的宽/画布的高
    that.camera.updateProjectionMatrix() // 更新摄像机投影矩阵。在任何参数被改变以后必须被调用,来使得这些改变生效
    that.renderer.setSize(that.viewerDom.clientWidth, that.viewerDom.clientHeight)
    that.renderer.setPixelRatio(window.devicePixelRatio) // 设置设备像素比
  }

  // 渲染dom
  _readerDom() {
    // 渲染如果报错，移除对象
    try {
      if (this.controlsEnabled) this.controls.update()
      this.renderer.render(this.scene, this.camera)
    } catch {
      this.destroy()
    }
  }

  // 添加skybox
  _initSkybox() {
    if (!this.skyboxs) this.skyboxs = new SkyBoxs(this)
    this.skyboxs.addSkybox()
  }

  // 灯光处理
  _initLight() {
    if (!this.lights) this.lights = new Lights(this)
  }

  flyTo(obj, duration = 1000) {
    const that = this
    // 创建一个临时对象，其中包含相机当前位置和目标点
    const temp = {
      cameraPoint: {...that.camera.position},
      cameraTarget: {...that.controls.target},
    }
    that.TWEEN.removeAll() // 清除所有活跃的tween
    // 解构目标位置
    const {cameraPoint: targetCameraPoint, cameraTarget: targetCameraTarget} = obj
    // 创建新的 TWEEN 动画
    new that.TWEEN.Tween(temp).to({
      cameraPoint: targetCameraPoint,
      cameraTarget: targetCameraTarget,
    }, duration).onUpdate(function (res) {
      // 动态改变相机位置
      if (that.camera) that.camera.position.set(res.cameraPoint.x, res.cameraPoint.y, res.cameraPoint.z)
      // 设置目标位置
      that.controls.target = new Vector3(res.cameraTarget.x, res.cameraTarget.y, res.cameraTarget.z)
    }).start()
  }
}
