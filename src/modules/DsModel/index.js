import * as THREE from 'three'
import {clone} from 'three/examples/jsm/utils/SkeletonUtils.js'
import DsModelSplit from './ModelSplit'

/**
 * 模型对象，带相关函数
 */
export default class DsModel {
  /**
   * 模型对象，带相关函数
   */
  constructor(_model, _viewer) {
    this.model = _model
    this.object = _model.scene // 模型内部的模型对象，不晓得threejs里面要叫scene,跟场景的scene不是一个东西
    this.viewer = _viewer
    this.animaIndex = -1 // 模型动画
    this.clock = this.viewer.clock // 全局时钟
    this.Materials = {}
    this.model.scene.traverse(model => {
      if (model.isMesh) this.Materials[model.id] = model.material
    })
  }

  /**
   * 从场景移除自己
   */
  remove() {
    this.object.parent.romove(this.object)
  }

  /**
   * 设置模型到原点位置
   */
  setCenter() {
    this.object.updateMatrixWorld()
    // 获得包围盒得min和max
    const box = new THREE.Box3().setFromObject(this.object)
    const center = box.getCenter(new THREE.Vector3())
    this.object.position.x = this.object.position.x - center.x
    this.object.position.y = 0
    this.object.position.z = this.object.position.z - center.z
  }

  /**
   * 设置模型比例
   * @param x 可以只填写一个参数
   * @param y 纵轴缩放
   * @param z 横轴缩放
   */
  setScale(x, y, z) {
    if (x) this.object.scale.x = x
    if (x || y) this.object.scale.y = y || x
    if (x || z) this.object.scale.z = z || x
  }

  /**
   * 设置模型缩放
   * @param x x横轴旋转
   * @param y 纵轴旋转
   * @param z z横轴旋转
   */
  setRotation(x, y, z) {
    if (x) this.object.rotation.x = x
    if (y) this.object.rotation.y = y
    if (z) this.object.rotation.z = z
  }

  /**
   * 设置模型位置
   * @param x x坐标
   * @param y y坐标
   * @param z z坐标
   * @param isRotation 是否根据传入坐标进行模型旋转
   * @param rotationY 默认模型朝向：Math.PI / 2
   */
  setPosition([x, y, z], isRotation = false, rotationY = Math.PI / 2) {
    // if (isRotation) {
    //     const zValue = z - this.object.position.z
    //     const xValue = x - this.object.position.x
    //     const angle = Math.atan2(zValue, xValue)
    //     this.object.rotation.y = rotationY - angle
    // }
    this.object.position.set(x, y, z)
  }

  /**
   * 克隆模型
   * @param x
   * @param y
   * @param z
   * @returns {*}
   */
  cloneModel([x, y, z] = [0, 0, 0]) {
    const newScene = {...this.model}
    const newModel = clone(this.object)
    newModel.position.set(x, y, z)
    this.viewer.scene.add(newModel)
    newScene.scene = newModel
    return new DsModel(newScene, this.viewer)
  }

  /**
   * 添加模型动画
   * @param i 选择模型动画进行播放
   */
  startAnima() {
    if (!this.mixer) this.mixer = new THREE.AnimationMixer(this.object)
    // 传入参数需要将函数与函数参数分开，在运行时填入
    this.animaObject = () => {
      this.mixer.update(this.clock.getDelta())
    }
    this.viewer.addAnimate(this.animaObject)
  }

  animaPlay(i = 0, callback) {
    this.animaIndex = i
    const anima = this.model.animations[i]
    const action = this.mixer.clipAction(anima)
    // 设置动画循环一次
    action.setLoop(THREE.LoopOnce)
    // 当动画完成后，执行回调函数
    action.clampWhenFinished = true // 保持动画在最后一帧
    // 开始播放动画
    action.play()

    this.mixer.addEventListener('finished', () => {
      this.mixer.removeEventListener('finished')
      callback(i)
    })
  }

  stopAnima() {
    const anima = this.model.animations[this.animaIndex]
    if (anima && this.mixer) this.mixer.clipAction(anima).stop()
  }

  /**
   * 移除模型动画
   */
  removeAnima() {
    if (this.animaObject) this.viewer.removeAnimate(this.animaObject)
  }

  _updateAnima(e) {

  }

  /**
   * 开启模型阴影 数组中移除阴影
   */
  openCastShadow(names = []) {
    this.model.scene.traverse(mesh => {
      if (mesh.isMesh) {
        if (names.length === 0) mesh.castShadow = true
        else if (names.indexOf(mesh.name) !== -1) mesh.castShadow = true
      }
    })
  }

  setVisible(visible) {
    this.object.children.forEach(_model => {
      _model.visible = visible
    })
  }

  /**
   * 根据名称设置模型可见
   * @param names
   */
  setVisibleByNames(names, visible) {
    console.log(names)
    names.forEach(name => {
      let mesh = this.object.getObjectByName(name)
      console.log(mesh)
      if (mesh) mesh.visible = visible
    })
  }

  setOpacityByNames(names, _option = {}) {
    const option = {
      color: 'rgb(255,255,255)',
      opacity: 0.05,
      ..._option,
    }
    const coolMaterial = new THREE.MeshBasicMaterial({
      transparent: true,
      depthWrite: false, // 无法被选择，鼠标穿透
      opacity: option.opacity,
      side: THREE.DoubleSide,
      ...option,
      color: new THREE.Color(option.color),
    })
    this.model.scene.traverse(model => {
      //先还原所有的材质
      if (model.isMesh) model.material = this.Materials[model.id]
    })
    names && names.forEach(name => {
      let mesh = this.object.getObjectByName(name)
      this.setChildMaterial(mesh, coolMaterial)
    })
  }

  setChildMaterial(obj, coolMaterial) {
    if (obj && obj.children && obj.children.length > 0) {
      obj.children.forEach(child => {
        this.setChildMaterial(child, coolMaterial)
      })
    } else {
      if (obj && obj.isMesh) obj.material = coolMaterial
    }
  }

  /**
   * 接收阴影
   * @param names 数组中的可以接收阴影
   */
  openReceiveShadow(names = []) {
    this.model.scene.traverse(mesh => {
      if (mesh.isMesh) {
        if (names.length === 0) mesh.receiveShadow = true
        else if (names.indexOf(mesh.name) !== -1) mesh.receiveShadow = true
      }
    })
  }

  /**
   * 获取模型集合
   * @param callback 返回模型集合
   */
  forEach(callback) {
    const temp = []
    this.model.scene.traverse(mesh => {
      if (mesh.isMesh) temp.push(mesh)
    })
    // 避免数据冲突
    temp.forEach(item => {
      callback && callback(item)
    })
  }

  /**
   * 关闭模型阴影
   */
  closeShadow() {
    this.model.scene.traverse(mesh => {
      mesh.castShadow = false
      mesh.receiveShadow = false
    })
  }

  /**
   * 设置模型炫酷 模式
   * @param option 颜色和透明度 颜色需要使用）0x 后面16进制颜色
   */
  setColorCool(names = null, color = 'rgb(1,123,224)', opacity = 0.05) {
    const that = this
    const coolMaterial = new THREE.MeshBasicMaterial({
      transparent: true,
      depthWrite: true, // 无法被选择，鼠标穿透
      color: new THREE.Color(color),
      opacity: opacity,
      side: THREE.DoubleSide,
    })
    this.model.scene.traverse(model => {
      if (model.isMesh && names && names.includes(model.name)) model.material = coolMaterial
      else if (model.isMesh) model.material = that.Materials[model.id]
    })
  }

  /**
   * 设置模型炫酷 模式
   * @param material 模型材质
   */
  setMaterialCool() {
    // 创建金属材质
    this.model.scene.traverse(model => {
      if (model.isMesh) {
        model.material = new THREE.MeshStandardMaterial({
          color: model.material.color, // 基本颜色
          roughness: 0.5, // 粗糙度，金属通常具有较低的粗糙度
          metalness: 0.8, // 金属度，设置为1表示完全是金属
          map: model.material.map,
          envMap: this.viewer.scene.background, // 环境贴图，用于反射周围环境
          side: THREE.DoubleSide,
        })
      }
    })
  }

  /**
   * 设置模型默认模式
   */
  setCoolDefault() {
    const that = this
    if (this.Materials && this.Materials.length > 0) {
      let i = 0
      this.model.scene.traverse(model => {
        if (model.isMesh) {
          model.material = that.Materials[i]
          i++
        }
      })
    }
  }

  /**
   * 设置模型炸开
   */
  startBomb() {
    if (!this.DsModelSplit) {
      this.DsModelSplit = new DsModelSplit()
      this.DsModelSplit.setSplitModel(this.object)
    }
    this.DsModelSplit.startSplit()
    // 传入参数需要将函数与函数参数分开，在运行时填入
    this.splitObject = () => this.DsModelSplit.update()
    this.viewer.addAnimate(this.splitObject)
    setTimeout(() => {
      this.viewer.removeAnimate(this.splitObject)
    }, 5000)
  }

  /**
   * 结束炸开模式
   */
  quitBomb() {
    this.DsModelSplit.quitSplit()
    // 传入参数需要将函数与函数参数分开，在运行时填入
    this.splitObject = () => this.DsModelSplit.update()
    this.viewer.addAnimate(this.splitObject)
    setTimeout(() => {
      this.viewer.removeAnimate(this.splitObject)
    }, 5000)
  }
}
