// RainPass.js
import { ShaderPass } from 'three/addons/postprocessing/ShaderPass.js';

class RainPass extends ShaderPass {
    constructor() {
        const rainShader = {
            uniforms: {
                tDiffuse: { value: null },
                time: { value: 0 },
                rainDropSize: { value: 0.02 },
                rainDropSpeed: { value: 0.3 }
            },
            vertexShader: `
                varying vec2 vUv;
                void main() {
                    vUv = uv;
                    gl_Position = projectionMatrix * modelViewMatrix * vec4(position, 1.0);
                }
            `,
            fragmentShader: `
                uniform sampler2D tDiffuse;
                uniform float time;
                uniform float rainDropSize;
                uniform float rainDropSpeed;
                varying vec2 vUv;

                void main() {
                    vec2 rainPos = vUv + vec2(0.0, time * rainDropSpeed);
                    float noise = texture2D(tDiffuse, rainPos).r;
                    float drop = smoothstep(0.0, rainDropSize, noise);
                    gl_FragColor = vec4(vec3(drop), 1.0);
                }
            `
        };

        super(rainShader);
    }

    update(time) {
        this.uniforms.time.value = time;
    }
}

export { RainPass };
