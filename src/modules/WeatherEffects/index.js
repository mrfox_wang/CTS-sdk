import * as THREE from 'three';
import {EffectComposer} from 'three/addons/postprocessing/EffectComposer.js';
import {RenderPass} from 'three/addons/postprocessing/RenderPass.js';
import {UnrealBloomPass} from 'three/addons/postprocessing/UnrealBloomPass.js';
import {RainPass} from './RainPass.js'; // 假设您有一个自定义的RainPass
import {SnowPass} from './SnowPass.js'; // 假设您有一个自定义的SnowPass
// import {FirePass} from './FirePass.js'; // 假设您有一个自定义的FirePass

/**
 * 天气效果类，用于实现雨天、雪天和火焰效果
 */
export default class WeatherEffects {
    constructor(_viewer) {
        this.viewer = _viewer;
        this.scene = _viewer.scene;
        this.camera = _viewer.camera;
        this.composer = new EffectComposer(_viewer.renderer);
        this.renderPass = new RenderPass(this.scene, this.camera);
        this.composer.addPass(this.renderPass);
        this.rainPass = null;
        this.snowPass = null;
        this.firePass = null;
        _viewer.afterReader(() => this.composer.render())
    }

    /**
     * 开启雨天效果
     */
    enableRain() {
        if (!this.rainPass) {
            this.rainPass = new RainPass();
            this.composer.addPass(this.rainPass);
        }
    }

    /**
     * 关闭雨天效果
     */
    disableRain() {
        if (this.rainPass) {
            this.composer.removePass(this.rainPass);
            this.rainPass = null;
        }
    }

    /**
     * 开启雪天效果
     */
    enableSnow() {
        if (!this.snowPass) {
            this.snowPass = new SnowPass();
            this.composer.addPass(this.snowPass);
        }
    }

    /**
     * 关闭雪天效果
     */
    disableSnow() {
        if (this.snowPass) {
            this.composer.removePass(this.snowPass);
            this.snowPass = null;
        }
    }

    /**
     * 开启火焰效果
     */
    enableFire() {
        if (!this.firePass) {
            this.firePass = new FirePass();
            this.composer.addPass(this.firePass);
        }
    }

    /**
     * 关闭火焰效果
     */
    disableFire() {
        if (this.firePass) {
            this.composer.removePass(this.firePass);
            this.firePass = null;
        }
    }
}
