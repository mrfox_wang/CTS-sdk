// SnowPass.js
import { ShaderPass } from 'three/addons/postprocessing/ShaderPass.js';

class SnowPass extends ShaderPass {
    constructor() {
        const snowShader = {
            uniforms: {
                tDiffuse: { value: null },
                time: { value: 0 },
                snowflakeSize: { value: 0.02 },
                snowflakeSpeed: { value: 0.2 }
            },
            vertexShader: `
                varying vec2 vUv;
                void main() {
                    vUv = uv;
                    gl_Position = projectionMatrix * modelViewMatrix * vec4(position, 1.0);
                }
            `,
            fragmentShader: `
                uniform sampler2D tDiffuse;
                uniform float time;
                uniform float snowflakeSize;
                uniform float snowflakeSpeed;
                varying vec2 vUv;

                void main() {
                    vec2 snowPos = vUv - vec2(0.0, time * snowflakeSpeed);
                    float noise = texture2D(tDiffuse, snowPos).r;
                    float flake = smoothstep(0.0, snowflakeSize, noise);
                    gl_FragColor = vec4(vec3(flake), 1.0);
                }
            `
        };

        super(snowShader);
    }

    update(time) {
        this.uniforms.time.value = time;
    }
}

export { SnowPass };
