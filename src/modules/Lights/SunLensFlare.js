import { Lensflare, LensflareElement } from 'three/examples/jsm/objects/Lensflare.js'
import * as THREE from 'three'

export default class SunLensFlare {
  /**
   * 太阳炫光
   * @param _viewer 视图
   */
  constructor(_viewer) {
    this.scene = _viewer.scene
    // lensflares 太阳炫光
    const textureLoader = new THREE.TextureLoader()
    this.textureFlare0 = textureLoader.load('resources/lensflare/lensflare0.png')
    this.textureFlare3 = textureLoader.load('resources/lensflare/hexangle.png')
  }

  /**
   * 添加炫光
   * @param x
   * @param y
   * @param z
   */
  addToScene(light) {
    // 构建炫光
    const lensflare = new Lensflare()
    lensflare.addElement(new LensflareElement(this.textureFlare0, 700, 0, light.color))
    lensflare.addElement(new LensflareElement(this.textureFlare3, 60, 0.6))
    lensflare.addElement(new LensflareElement(this.textureFlare3, 70, 0.7))
    lensflare.addElement(new LensflareElement(this.textureFlare3, 120, 0.9))
    lensflare.addElement(new LensflareElement(this.textureFlare3, 70, 1))
    light.add(lensflare)
  }
}
