import * as THREE from 'three'
import SunLensFlare from './SunLensFlare'

export default class Lights {
  constructor(_viewer) {
    this.viewer = _viewer
    this.lightGroup = new THREE.Group()
    this.viewer.scene.add(this.lightGroup)
    this.sunLensFlare = new SunLensFlare(_viewer)
    this._initLight()
  }

  _initLight() {
    const hemiLight = new THREE.HemisphereLight(0xffffff, 0xffffff, 2)
    hemiLight.color.setHSL(0.6, 1, 0.6)
    hemiLight.groundColor.setHSL(0.095, 1, 0.75)
    hemiLight.position.set(0, 50, 0)
    this.viewer.scene.add(hemiLight)
    this.hemiLight = hemiLight

    // this.hemiLightHelper = new THREE.HemisphereLightHelper(hemiLight, 10)
    // this.lightGroup.add(this.hemiLightHelper)

    //

    this.dirLight = new THREE.DirectionalLight(0xffffff, 10)
    const dirLight = this.dirLight
    dirLight.color.setHSL(0.1, 1, 0.95)
    dirLight.position.set(-10, 15, 20)
    dirLight.position.multiplyScalar(30)
    this.lightGroup.add(this.dirLight)
    dirLight.castShadow = true
    dirLight.shadow.mapSize.width = 4096
    dirLight.shadow.mapSize.height = 4096
    const d = 300
    dirLight.shadow.camera.left = -d
    dirLight.shadow.camera.right = d
    dirLight.shadow.camera.top = d
    dirLight.shadow.camera.bottom = -d
    dirLight.shadow.camera.far = 1500 // 可以根据场景大小调整这个值
    dirLight.shadow.bias = -0.003
    this.sunLensFlare.addToScene(dirLight)
    this.dirLightHelper = new THREE.DirectionalLightHelper(dirLight, 0)
    this.lightGroup.add(this.dirLightHelper)
    this.ambientLight = new THREE.AmbientLight(0x0c0c0c, 50) // 创建环境光
    this.lightGroup.add(this.ambientLight) // 将环境光添加到场景

    const pointLight = new THREE.SpotLight(new THREE.Color('rgb(137,208,224)'), 100, 100)
    // pointLight.castShadow = true
    pointLight.position.set(0, 20, 0)
    pointLight.intensity = 20 // 光线强度
    pointLight.decay = 1 // 光的衰减指数
    pointLight.penumbra = 1
    this.lightGroup.add(pointLight) // 将环境光添加到场景
    this.pointLight = pointLight
  }

  setPLight(option) {
    this.pointLight.visible = option.visible
    this.pointLight.position.set(option.position.x, option.position.y, option.position.z)
    this.pointLight.intensity = option.intensity
    this.pointLight.decay = option.decay
    this.pointLight.penumbra = option.penumbra
  }

  setHLight(option) {
    this.hemiLight.visible = option.visible
    this.hemiLight.position.set(option.position.x, option.position.y, option.position.z)
    // this.hemiLightHelper.visible = option.helper
    this.dirLight.intensity = option.intensity
  }

  setDLight(option) {
    this.dirLight.visible = option.visible
    this.dirLightHelper.visible = option.helper
    this.dirLight.intensity = option.intensity
    this.dirLight.castShadow = option.castShadow
    this.dirLight.position.set(option.position.x, option.position.y, option.position.z)
    this.dirLight.shadow.mapSize.width = option.size
    this.dirLight.shadow.mapSize.height = option.size
    const d = option.padding
    this.dirLight.shadow.camera.left = -d
    this.dirLight.shadow.camera.right = d
    this.dirLight.shadow.camera.top = d
    this.dirLight.shadow.camera.bottom = -d
    this.dirLight.shadow.camera.far = option.far // 可以根据场景大小调整这个值
    this.dirLight.shadow.camera.far = option.far // 可以根据场景大小调整这个值
    this.dirLight.shadow.bias = -option.bias
    this.dirLight.shadow.radius = option.radius
  }

  setALight(option) {
    this.ambientLight.visible = option.visible
    this.ambientLight.intensity = option.intensity
  }

}
