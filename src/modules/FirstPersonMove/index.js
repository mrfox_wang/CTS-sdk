import * as THREE from 'three'
import THREEx from './keyboardState'
import {Capsule, Octree} from "three/addons";

export default class FirstPersonMove {
    constructor(_viewer) {
        this.viewer = _viewer
        this.scene = _viewer.scene
        // 控制代码
        this.keyboard = new THREEx.KeyboardState()
        this.clock = new THREE.Clock()
        //场景管理
        this.playerVelocity = new THREE.Vector3();
        this.playerDirection = new THREE.Vector3();
        this.playerOnFloor = false;
        this.model = undefined
    }

    _controls(deltaTime) {
        const speedDelta = deltaTime * (this.playerOnFloor ? 25 : 8);
        if (this.keyboard.pressed('w')) {
            this.playerVelocity.add(this.getForwardVector().multiplyScalar(speedDelta));
        }
        if (this.keyboard.pressed('s')) {
            this.playerVelocity.add(this.getForwardVector().multiplyScalar(-speedDelta));
        }
        if (this.keyboard.pressed('a')) {
            this.playerVelocity.add(this.getSideVector().multiplyScalar(-speedDelta));
        }
        if (this.keyboard.pressed('d')) {
            this.playerVelocity.add(this.getSideVector().multiplyScalar(speedDelta));
        }
        if (this.keyboard.pressed('Space') && this.playerOnFloor) {
            this.playerVelocity.y = 15;
        }
    }

    _updateCamera() {
        const cameraOffset = new THREE.Vector3(0, 4, -1).applyMatrix4(this.model.matrixWorld)
        this.viewer.camera.position.x = cameraOffset.x
        this.viewer.camera.position.y = cameraOffset.y
        this.viewer.camera.position.z = cameraOffset.z
        // 视线在前面
        const cameraBackOffset = new THREE.Vector3(0, 2, 2).applyMatrix4(this.model.matrixWorld)
        this.viewer.lookAt(cameraBackOffset)
    }


    getForwardVector() {
        this.viewer.camera.getWorldDirection(this.playerDirection);
        this.playerDirection.y = 0;
        this.playerDirection.normalize();
        return this.playerDirection;
    }

    getSideVector() {
        this.viewer.camera.getWorldDirection(this.playerDirection);
        this.playerDirection.y = 0;
        this.playerDirection.normalize();
        this.playerDirection.cross(this.viewer.camera.up);
        return this.playerDirection;
    }

    /**
     * 设置第三人称人物
     * @param model
     */
    start(model, option = {}) {
        this.do = () => {
            this._controls()
        }
        document.onkeydown = event => {
            this.viewer.addAnimate(this.do) // 添加到全局动画
        }
        document.onkeyup = event => {

        }
    }

    stop() {
        this.viewer.removeAnimate(this.do) // 移除动画
    }

    isKey(key) {
        const keys = ['w', 'a', 's', 'd', 'ArrowRight', 'ArrowLeft']
        return keys.includes(key)
    }
}
