import * as THREE from 'three'
import THREEx from './keyboardState'

export default class FirstPersonMove {
  constructor (_viewer) {
    this.viewer = _viewer
    this.scene = _viewer.scene
    // 控制代码
    this.keyboard = new THREEx.KeyboardState()
    this.clock = new THREE.Clock()
  }

  _event (that) {
    const delta = that.clock.getDelta()
    const moveDistance = that.speed * delta
    const rotateAngle = Math.PI / 2 * delta
    if (that.keyboard.pressed('s')) {
      that.firstPerson.translateZ(-moveDistance)
      that.dsModel.animaPlay(that.moveIndex)
    }
    if (that.keyboard.pressed('w')) {
      that.firstPerson.translateZ(moveDistance)
      that.dsModel.animaPlay(that.moveIndex)
    }
    if (that.keyboard.pressed('a')) {
      that.firstPerson.translateX(moveDistance)
      that.dsModel.animaPlay(that.moveIndex)
    }
    if (that.keyboard.pressed('d')) {
      that.firstPerson.translateX(-moveDistance)
      that.dsModel.animaPlay(that.moveIndex)
    }
    if (that.keyboard.pressed('left')) {
      that.firstPerson.rotateOnAxis(new THREE.Vector3(0, 1, 0), rotateAngle)
      that.dsModel.animaPlay(that.moveIndex)
    }
    if (that.keyboard.pressed('right')) {
      that.firstPerson.rotateOnAxis(new THREE.Vector3(0, 1, 0), -rotateAngle)
      that.dsModel.animaPlay(that.moveIndex)
    }
    // 相机视角
    const relativeCameraOffset = new THREE.Vector3(that.position[0], that.position[1], that.position[2])
    const cameraOffset = relativeCameraOffset.applyMatrix4(that.firstPerson.matrixWorld)
    that.viewer.camera.position.x = cameraOffset.x
    that.viewer.camera.position.y = cameraOffset.y
    that.viewer.camera.position.z = cameraOffset.z
    // 视线在前面
    const cameraBackOffset = new THREE.Vector3(that.position[0], that.position[1], -that.position[2]).applyMatrix4(that.firstPerson.matrixWorld)
    that.viewer.controls.target = cameraBackOffset
  }

  // changeModelRo(model){
  //   // 计算模型角度
  //   const zValue = position[2] - this.model.position.z
  //   const xValue = position[0] - this.model.position.x
  //   const angle = Math.atan2(zValue, xValue)
  //   this.model.rotation.y = this.rotationY - angle
  //   this.model.position.set(position[0], position[1], position[2])
  // }

  /**
   * 设置第三人称人物
   * @param model
   */
  start (_dsModel, _position, _speed, _moveIndex) {
    this.dsModel = _dsModel
    this.position = _position
    this.dsModel.startAnima()
    this.firstPerson = _dsModel.object
    this.speed = _speed
    this.moveIndex = _moveIndex
    this.do = { fun: this._event, content: this }
    this.viewer.addAnimate(this.do) // 添加到全局动画
    document.onkeydown = event => {
      if (event.key === 'esc') {
        this.dsModel.stopAnima()
        this.stop()
      }
    }
    document.onkeyup = event => {
      if (this.isKey(event.key)) this.dsModel.stopAnima()
    }
  }

  stop () {
    this.viewer.removeAnimate(this.do) // 移除动画
  }

  isKey (key) {
    const keys = ['w', 'a', 's', 'd', 'ArrowRight', 'ArrowLeft']
    return keys.includes(key)
  }
}
