import * as THREE from 'three'
import * as d3 from "d3";
import {Mesh, MeshPhongMaterial, PlaneGeometry, RepeatWrapping, TextureLoader} from "three";
import {cylinderMaterial, mashMaterial} from './material'
import {Line2, LineGeometry, LineMaterial} from "three/addons";

export default class GeoJsonLoader {
    constructor(_viewer, geoJson, _option) {
        this.viewer = _viewer
        this.option = {center: [116.3975, 39.909], height: 5, up: true, showLine: false, ..._option}
        //坐标转换器
        this.projection = d3.geoMercator().center(this.option.center).scale(1400).translate([0, 0]);
        this.GeoGroup = new THREE.Object3D();

        //面的组
        this.meshGroup = new THREE.Object3D();
        this.GeoGroup.add(this.meshGroup)
        if (this.option.up) this.meshGroup.rotation.x = -Math.PI / 2
        //线的组
        this.lineGroup = new THREE.Object3D();
        this.GeoGroup.add(this.lineGroup)
        this.viewer.scene.add(this.GeoGroup)
        if (this.option.up) this.lineGroup.rotation.x = -Math.PI / 2
        this._createMaterial()
        this._loadGeoJson(geoJson.features)
    }

    _createMaterial() {
        //构建贴图
        const {repeatX, repeatY, offsetX, offsetY, meshMaterial, sideColor} = this.option
        // 随机颜色
        const materialFront = new MeshPhongMaterial({
            map: new TextureLoader().load(meshMaterial || mashMaterial, function (texture) {
                texture.wrapS = texture.wrapT = RepeatWrapping
                // uv两个方向纹理重复数量
                texture.repeat.set(repeatX || 0.05, repeatY || 0.05)
                texture.offset.x -= offsetX || 0
                texture.offset.y -= offsetY || 0
                // 需要把色彩空间编码改一下，原因我上一篇说过的
                texture.encoding = THREE.sRGBEncoding
            }),
            transparent: true,
            depthTest: true,
        })
        materialFront.map.colorSpace = 'srgb'
        const materialSide = new THREE.MeshBasicMaterial({color: sideColor});
        this.materials = [materialFront, materialSide]
    }

    /**
     * 加载·geojson
     * @param features 标准的geojson数据
     * @private
     */
    _loadGeoJson(features) {
        features.forEach((feature) => {
            const coordinates = feature.geometry.coordinates;
            const {name} = feature.properties
            const group = new THREE.Object3D();
            group.name = name
            if (feature.geometry.type == "MultiPolygon") {
                coordinates.forEach((coordinate) => {
                    this._createMeshLine(name, coordinate, group)
                })
            }
            if (feature.geometry.type == "Polygon") {
                this._createMeshLine(name, coordinates, group)
            }
            //创建文字标签
            // let txt = this._drawTxt(name, feature)
            // group.add(txt)
            // //绘制圆柱
            // let cylinder = this._drawCylinder(name, feature)
            // group.add(cylinder)
            this.meshGroup.add(group)
        })
    }

    loadLineGeoJson(features) {
        const {height} = this.option
        features.forEach((feature) => {
            const coordinates = feature.geometry.coordinates;
            console.log(feature.properties);
            const line = this.lineDraw(coordinates, height + 0.01)
            this.lineGroup.add(line)
        })
    }

    _createMeshLine(name, coordinates, group) {
        coordinates.forEach((polygon) => {
            //底部面
            const mesh = this.drawExtrudeMesh(polygon)
            mesh.name = name
            group.add(mesh)

            //顶部线段
            const {height, offset} = this.option
            const line = this.lineDraw(polygon, height, offset)
            line.name = name
            group.add(line)

            const line1 = this.lineDraw(polygon)
            this.lineGroup.add(line1)
        })
    }

    _drawCylinder(name, feature) {
        const group = new THREE.Object3D();
        const [x, y] = this.projection(feature.properties.center); // 假设projection方法可以返回圆柱中心的坐标
        const heightg = Math.floor(Math.random() * 4) + 1
        let material = new THREE.MeshBasicMaterial({
            color: "#8FFFFF",
            transparent: true, //设置透明
            map: new TextureLoader().load(cylinderMaterial, function (texture) {
                texture.wrapS = texture.wrapT = RepeatWrapping
                // uv两个方向纹理重复数量
                texture.repeat.set(1, 1)
                // 需要把色彩空间编码改一下，原因我上一篇说过的
                texture.encoding = THREE.sRGBEncoding
            }),
            side: THREE.DoubleSide,
            depthWrite: false
        })
        let planeGeometry = new PlaneGeometry(1, heightg)
        let plane = new Mesh(planeGeometry, material)
        // 设置圆柱体的位置
        plane.position.set(x, -y, heightg / 2); // 假设y坐标需要取反
        plane.rotation.x = Math.PI / 2
        // 设置网格的renderOrder
        plane.renderOrder = 1; // 设置为1，这意味着它将在默认的renderOrder为0的对象之后渲染
        group.add(plane)
        let plane1 = plane.clone()
        plane1.rotation.y = Math.PI / 3
        group.add(plane1)
        let plane2 = plane.clone()
        plane2.rotation.y = -Math.PI / 3
        group.add(plane2)
        return group; // 返回圆柱体对象，以便后续操作
    }

    _drawTxt(name, feature) {
        if (!feature.properties.center) return
        const {height} = this.option
        const [x, y] = this.projection(feature.properties.center);
        let div = document.createElement('div')
        div.style.pointerEvents = 'all'
        div.style.cursor = 'pointer'
        div.style.color = '#FFF'
        div.style.fontWeight = "600";
        div.style.fontSize = '12px'
        div.style.userSelect = 'none'
        div.innerHTML = name
        let label2d = this.option.labels.addDivLabel(div, [x, -y, height], '2d', false)
        return label2d
    }

    lineDraw(polygon, height = 0, offset = 0, option = {}) {
        const lineGeometry = new LineGeometry();
        const pointsArray = [];
        polygon.forEach((row) => {
            const [x, y] = this.projection(row);
            // 创建三维点
            pointsArray.push(x, -y, height + offset);
        });
        // 放入多个点
        lineGeometry.setPositions(pointsArray);
        const lineMaterial = new LineMaterial({
            color: option.lineColor || this.option.lineColor || 0xffffff,
            linewidth:  option.linewidth || this.option.linewidth || 1,
        });
        const line = new Line2(lineGeometry, lineMaterial);
        line.computeLineDistances()
        return line
    }

    drawExtrudeMesh(polygon) {
        const shape = new THREE.Shape();
        polygon.forEach((row, i) => {
            const [x, y] = this.projection(row);
            if (i === 0) {
                // 创建起点,使用moveTo方法
                // 因为计算出来的y是反过来，所以要进行颠倒
                shape.moveTo(x, -y);
            }
            shape.lineTo(x, -y);
        });
        // 拉伸
        const geometry = new THREE.ExtrudeGeometry(shape, {
            depth: this.option.height * 3,
            bevelEnabled: false,
        });
        const mesh = new THREE.Mesh(geometry, this.materials);
        mesh.position.z = -this.option.height * 2
        return mesh
    }
}
