import { PointerLockControls } from 'three/examples/jsm/controls/PointerLockControls'
import * as THREE from 'three'
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls.js'

export default class FPS {
  constructor(_viewer, _option = {}) {
    this.viewer = _viewer
    this.viewer.controlsEnabled = false
    this.controls = new PointerLockControls(this.viewer.camera, this.viewer.viewerDom)
    this.viewer.scene.add(this.controls.getObject())
    this.controls.getObject().position.set(0, 1.7, 0)
    this.option = { speed: 5, ..._option }
    this.raycaster = new THREE.Raycaster()
    this._setupEventListeners()
  }

  _setupEventListeners() {
    this.keyMap = {}
    this.lock = () => this.controls.lock()
    this.viewer.viewerDom.addEventListener('click', this.lock)
    this.mouselock = () => (this.viewer.viewerDom.style.display = 'none')
    this.viewer.viewerDom.addEventListener('lock', this.mouselock)
    this.block = () => (this.viewer.viewerDom.style.display = 'block')
    this.viewer.viewerDom.addEventListener('unlock', this.block)
    document.addEventListener('keydown', this._onKeyChange.bind(this), false)
    document.addEventListener('keyup', this._onKeyChange.bind(this), false)
  }

  _onKeyChange(e) {
    this.keyMap[e.code] = e.type === 'keydown'
  }

  start() {
    this.move = () => {
      const delta = this.viewer.clock.getDelta()
      this._handleMovement(delta)
    }
    this.viewer.addAnimate(this.move)
  }

  close() {
    this.viewer.removeAnimate(this.move)
    this.viewer.viewerDom.removeEventListener('click', this.lock)
    this.viewer.viewerDom.removeEventListener('lock', this.mouselock)
    this.viewer.viewerDom.removeEventListener('unlock', this.block)
    document.removeEventListener('keydown', this._onKeyChange.bind(this), false)
    document.removeEventListener('keyup', this._onKeyChange.bind(this), false)
    this.viewer.scene.remove(this.controls.getObject())
    this.controls = new OrbitControls(this.viewer, this.viewer.domElement)
  }

  _handleMovement(delta) {
    const moveAmount = delta * this.option.speed
    if (this.keyMap['KeyW'] || this.keyMap['ArrowUp']) {
      this._moveForward(moveAmount)
    }
    if (this.keyMap['KeyS'] || this.keyMap['ArrowDown']) {
      this._moveForward(-moveAmount)
    }
    if (this.keyMap['KeyA'] || this.keyMap['ArrowLeft']) {
      this._moveRight(-moveAmount)
    }
    if (this.keyMap['KeyD'] || this.keyMap['ArrowRight']) {
      this._moveRight(moveAmount)
    }
    if (this.keyMap['KeyQ']) {
      this.controls.getObject().position.y -= moveAmount
    }
    if (this.keyMap['KeyE']) {
      this.controls.getObject().position.y += moveAmount
    }
  }

  _moveForward(amount) {
    this.controls.moveForward(amount)
  }

  _moveRight(amount) {
    this.controls.moveRight(amount)
  }
}
