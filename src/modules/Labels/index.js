import { CSS2DRenderer, CSS2DObject } from 'three/examples/jsm/renderers/CSS2DRenderer.js'
import { CSS3DObject, CSS3DRenderer } from 'three/examples/jsm/renderers/CSS3DRenderer'
import { Group, Raycaster, Sprite, SpriteMaterial, TextureLoader, Vector3 } from 'three'

export default class Labels {
  constructor(_viewer) {
    this.viewer = _viewer
    this.labelGroup = new Group()
    this.spriteGroup = new Group()
    _viewer.scene.add(this.labelGroup)
    _viewer.scene.add(this.spriteGroup)
    this.raycaster = new Raycaster() // 初始化射线投射器
    this.objectsToTest = _viewer.scene.children.filter(child => child.isMesh) // 获取需要测试的物体
    this._initRender() // 初始化渲染
    // eslint-disable-next-line @typescript-eslint/no-this-alias
    // const that = this
    // this.viewer.viewerDom.addEventListener('mousemove', ()=>{
    //   that._checkIntersection()
    // })
  }

  removeLabelAll() {
    this.labelGroup.remove(...this.labelGroup.children)
  }

  removeSpriteAll() {
    this.spriteGroup.remove(...this.spriteGroup.children)
  }

  /**
   * 渲染2d 3D样式渲染器
   * @private
   */
  _initRender() {
    // 实例化CSS2DRenderer 。labelRenderer在函数外面定义
    const _2DRenderer = new CSS2DRenderer()
    _2DRenderer.setSize(this.viewer.viewerDom.clientWidth, this.viewer.viewerDom.clientHeight)
    this._setStyle(_2DRenderer)
    this.viewer.viewerDom.appendChild(_2DRenderer.domElement)// 一个canvas，渲染器在其上绘制输出。
    const _3dRenderer = new CSS3DRenderer()
    this._setStyle(_3dRenderer)
    this.viewer.viewerDom.appendChild(_3dRenderer.domElement)// 一个canvas，渲染器在其上绘制输出。

    // eslint-disable-next-line @typescript-eslint/no-this-alias
    const that = this
    this.animateRender = () => {
      // 更新并渲染CSS2DRenderer
      _2DRenderer.render(this.viewer.scene, this.viewer.camera)
      _2DRenderer.setSize(this.viewer.viewerDom.clientWidth, this.viewer.viewerDom.clientHeight)
      _3dRenderer.render(this.viewer.scene, this.viewer.camera)
      _3dRenderer.setSize(this.viewer.viewerDom.clientWidth, this.viewer.viewerDom.clientHeight)
      //监测三维图标是否被遮挡
      // that._checkIntersection() // 在渲染循环中调用遮挡检测
    }
    this.viewer.addAnimate(this.animateRender)
  }

  _checkIntersection() {
    this.labelGroup.children.forEach(child => {
      // 定义射线的起点和终点
      const origin = child.position.clone() // 起点坐标
      const end = this.viewer.camera.position.clone() // 终点坐标
      // 计算方向向量（从起点指向终点）
      const direction = new Vector3().subVectors(end, origin).normalize()
      // 创建射线投射器
      const raycaster = new Raycaster(origin, direction)
      raycaster.far = origin.distanceTo(end)
      // 获取射线与物体相交的结果
      const intersects = raycaster.intersectObjects(this.viewer.scene.children)
      if (intersects && intersects.length > 0) child.visible = false
      else child.visible = true
    })
  }

  /**
   * 设置样式
   * @param renderer
   * @private
   */
  _setStyle(renderer) {
    //设置2d渲染器布局
    renderer.domElement.style.position = 'absolute'
    renderer.domElement.style.top = '0px'
    renderer.domElement.style.pointerEvents = 'none'
  }

  /**
   * 添加标签
   * @param div
   * @param x
   * @param y
   * @param z
   * @param callback
   * @param type
   * @returns {CSS2DObject|CSS3DObject}
   */
  addDivLabel(option, callback) {
    const { div, position, rotation, type, exData } = option
    div.style.pointerEvents = 'all'
    div.style.cursor = 'pointer'
    div.style.userSelect = 'none'
    div.blur() //禁止被聚焦
    let label = new CSS2DObject(div)
    if (type == '3d') {
      label = new CSS3DObject(div)
      label.rotation.set(rotation.x, rotation.y, rotation.z)
      label.scale.set(0.05, 0.05, 0.05)
    }
    label.position.set(position.x, position.y, position.z)
    this.labelGroup.add(label)
    div.addEventListener('pointerdown', () => {
      if (callback) callback(exData)
    })
    return label
  }

  addSprite(url, poi = [0, 0, 0], scale = [1, 1, 1], add = true) {
    let map = new TextureLoader().load(url)
    let material = new SpriteMaterial({ map: map })
    material.map.colorSpace = 'srgb'
    const sprite = new Sprite(material)
    sprite.position.set(poi[0], poi[1], poi[2])
    sprite.scale.set(scale[0], scale[1], scale[2])
    this.spriteGroup.add(sprite)
    return sprite
  }
}
