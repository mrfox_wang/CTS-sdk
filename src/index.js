/**
 * @Author: wangrui
 * 炫景 - City Twins Scenes
 * @Date: 2022-06-15 16:45:45
 */
import * as THREE from 'three'
import modules from './modules'

/**
 * 初始化库
 * @type {{author: string, version: string, threeJSLoaded: boolean}}
 */
const CTS = {
    version: '1.0.0',
    author: 'mrfoxWang',
    threeJSLoaded: false
}

/**
 * 初始化函数
 * @param callback 资源加载完毕
 * @constructor
 */
CTS.ready = (callback) => {
    if (CTS.threeJSLoaded) {
        callback && callback()
    } else {
        // 挂接THREE属性
        for (const k in THREE) {
            CTS[k] = THREE[k]
        }
        // 挂接对象属性
        for (const k in modules) {
            CTS[k] = modules[k]
        }
        CTS.threeJSLoaded = true
        callback && callback()
    }
}

export default CTS
